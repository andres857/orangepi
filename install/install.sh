#!/bin/bash


###login the user
#sudo apt-get update && sudo apt-get upgrade -y 

##agregar permisos
echo 'PATH="$PATH:/sbin"' >> ~/.profile 2>>/home/machine/errorinstall.log

mkdir ~/Project 2>>/home/machine/errorinstall.log

mkdir ~/Project/logs 2>>/home/machine/errorinstall.log
mkdir ~/.aws 2>>/home/machine/errorinstall.log
#mkdir ~/configAWS

##Descargar y el cliente de AWS
cd ~/
wget https://s3.amazonaws.com/aws-cli/awscli-bundle.zip 2>>/home/machine/errorinstall.log
unzip awscli-bundle.zip 2>>/home/machine/errorinstall.log
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws 2>>/home/machine/errorinstall.log

rm awscli-bundle.zip 2>>/home/machine/errorinstall.log
rm -rf awscli-bundle/ 2>>/home/machine/errorinstall.log


###descargar las llaves y configurar el aws

git clone git@gitlab.com:andres857/orangepi.git
mv ~/orangepi/files/configure ~/.aws 2>>/home/machine/errorinstall.log
sudo chmod 400 /home/machine/orangepi/files/firtskey.pem  2>>/home/machine/errorinstall.log
sudo chmod +x ~/orangepi/files/monitoreo.sh 2>>/home/machine/errorinstall.log
sudo chmod +x ~/orangepi/files/updata.sh 2>>/home/machine/errorinstall.log

cd ~/orangepi/files 2>>/home/machine/errorinstall.log
tar -xvf pyserial-2.7.tar.gz 2>>/home/machine/errorinstall.log
cd pyserial-2.7 2>>/home/machine/errorinstall.log
sudo python setup.py install 2>>/home/machine/errorinstall.log


mkdir ~/Project/logs/daily 2>>/home/machine/errorinstall.log
mkdir ~/Project/logs/finished 2>>/home/machine/errorinstall.log

#echo "* * * * * ~/Project/monitoreo.sh" >>/usr/bin/crontab
##Instalar node.js
curl -o node-v9.7.1-linux-armv6l.tar.gz https://nodejs.org/dist/v9.7.1/node-v9.7.1-linux-armv6l.tar.gz
tar -xzf node-v9.7.1-linux-armv6l.tar.gz
sudo cp -r node-v9.7.1-linux-armv6l/* /usr/local/
rm node-v9.7.1-linux-armv6l.tar.gz
rm -rf node-v9.7.1-linux-armv6l/
##comprobar la instalacion
node -v
npm -v
