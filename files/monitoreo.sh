#!/bin/bash
#touch estadodelared.log
Up=UpNetwork;
Down=DownNetwork;

Signal=$(iwconfig wlan0 | grep -i Signal)
t=$(cat /sys/devices/virtual/thermal/thermal_zone0/temp)


if ping -c1 www.google.com 1>/dev/null;

then
echo $(date +'%F,%T'),$Up,$t >>~/Project/logs/daily/$(date +'%F'_)estado.log
else 
echo $(date +'%F,%T'),$Down,$t >>~/Project/logs/daily/$(date +'%F'_)estado.log
fi
